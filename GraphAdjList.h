#ifndef COSC2131_GRAPH_S3445846_GRAPHADJLIST_H
#define COSC2131_GRAPH_S3445846_GRAPHADJLIST_H

#include <iostream>
#include <cstdlib>
#include <stdexcept>
#include <list>
#include "Graph.h"

using namespace std;

template<typename W>
class GraphAdjList : public Graph<W> {
protected:
    // Adjacency List Node
    struct AdjListNode {
        int index;
        W value;
        struct AdjListNode* next;
    };

    // Adjacency List
    struct AdjList {
        struct AdjListNode *head;
    };

    struct AdjList* array;

public:
    const W INVALID_WEIGHT;

    GraphAdjList(int n_vertices, W invalidWeight) : Graph<W>::Graph(n_vertices), INVALID_WEIGHT(invalidWeight) {
        array = new AdjList [n_vertices];
        for (int i = 0; i < n_vertices; ++i)
            array[i].head = NULL;
    }

    // Create new node.
    AdjListNode* newAdjListNode(int index, int value) {
        AdjListNode* newNode = new AdjListNode;
        newNode->value = value;
        newNode->index = index;
        newNode->next = NULL;
        return newNode;
    }

    virtual W &getWeight(const int from, const int to) const {
        AdjListNode *node = array[from].head;
        while (node) {
            if (node->index == to) {
                return node->value;
            }
            node = node->next;
        }
    }

    virtual void setWeight(const int from, const int to, const W &weight) {
        AdjListNode *newNode = newAdjListNode(to, weight);
        newNode->next = array[from].head;
        array[from].head = newNode;
    }

    virtual bool hasEdge(const int from, const int to) const {
        AdjListNode *node = array[from].head;
        while (node) {
            if (node->index == to) {
                return 1;
            }
            node = node->next;
        }

        return 0;
    }

    // Breadth first search.
    virtual void BFS(int s) const {
        cout << "Breadth first search: ";

        // Mark all the vertices as not visited
        bool *visited = new bool[this->n_vertices];
        for(int i = 0; i < this->n_vertices; i++)
            visited[i] = false;

        // Create a queue for BFS
        list<int> queue;

        // Mark the current node as visited and enqueue it
        visited[s] = true;
        queue.push_back(s);

        cout << "Nodes: ";
        while(!queue.empty()) {
            // Dequeue a vertex from queue and print it
            s = queue.front();
            cout << s << " ";
            queue.pop_front();

            // Get all adjacent vertices of the dequeued vertex s
            // If a adjacent has not been visited, then mark it visited
            // and enqueue it
            for (int i = 0; i < this->n_vertices; ++i) {
                AdjListNode* node = array[s].head;
                while (node) {
                    if (!visited[node->index]) {
                        visited[node->index] = true;
                        queue.push_back(node->index);
                    }
                    node = node->next;
                }
            }
        }
        cout << endl;
    }

    // Deapth first search.
    void DFSUtil(int s, bool visited[]) const {
        // Mark the current node as visited and print it
        visited[s] = true;
        cout << s << " ";

        // Recur for all the vertices adjacent to this vertex
        for (int i = 0; i < this->n_vertices; ++i) {
            AdjListNode *node = array[s].head;
            while (node) {
                if (!visited[node->index]) {
                    DFSUtil(node->index, visited);
                }
                node = node->next;
            }
        }
    }

    virtual void DFS(int s) const {
        cout << "Depth first search: ";
        // Mark all the vertices as not visited
        bool *visited = new bool[this->n_vertices];
        for (int i = 0; i < this->n_vertices; i++)
            visited[i] = false;

        // Call the recursive helper function to print DFS traversal
        cout << "Nodes: ";
        DFSUtil(s, visited);
        cout << endl;
    }

    // Topological sort.
    void topologicalSortUtil(int s, bool visited[], stack<int> &Stack) const {
        // Mark the current node as visited.
        visited[s] = true;

        // Recur for all the vertices adjacent to this vertex
        for (int i = 0; i < this->n_vertices; ++i) {
            AdjListNode *node = array[s].head;
            while (node) {
                if (!visited[node->index]) {
                    topologicalSortUtil(node->index, visited, Stack);
                }
                node = node->next;
            }
        }

        // Push current vertex to stack which stores result
        Stack.push(s);
    }

    virtual void topologicalSort() const {
        cout << "Topological sort: ";
        stack<int> Stack;

        // Mark all the vertices as not visited
        bool *visited = new bool[this->n_vertices];
        for (int i = 0; i < this->n_vertices; i++)
            visited[i] = false;

        // Call the recursive helper function to store Topological
        // Sort starting from all vertices one by one
        for (int i = 0; i < this->n_vertices; i++)
            if (visited[i] == false)
                topologicalSortUtil(i, visited, Stack);

        // Print contents of stack
        cout << "Nodes: ";
        while (Stack.empty() == false) {
            cout << Stack.top() << " ";
            Stack.pop();
        }
        cout << endl;
    }

    // Print the matrix.
    friend ostream&operator << (ostream &out, GraphAdjList<W> &g) {
        for (int i = 0; i < g.n_vertices; ++i) {
            AdjListNode* node = g.array[i].head;
            out << "Vertex " << i;
            while (node) {
                out << " -> " << node->index << "(" << node->value << ")";
                node = node->next;
            }
            out << endl;
        }

        return out;
    }
};

#endif //COSC2131_GRAPH_S3445846_GRAPHADJLIST_H

#ifndef GRAPH_MATRIXGRAPH_H
#define GRAPH_MATRIXGRAPH_H

#include <iostream>
#include <stdexcept>
#include <list>
#include <stack>
#include "Graph.h"

using namespace std;

template<typename W>
class GraphMatrix : public Graph<W> {
public:
    const W INVALID_WEIGHT;

    GraphMatrix(int n_vertices, W invalidWeight) : Graph<W>::Graph(n_vertices), INVALID_WEIGHT{invalidWeight} {
        length = this->n_vertices * this->n_vertices;
        matrix = new W[length];
        for (int i = 0; i < length; i++) {
            matrix[i] = INVALID_WEIGHT;
        }
    }

    virtual W &getWeight(const int from, const int to) const {
        return matrix[from * this->n_vertices + to];
    }

    virtual void setWeight(const int from, const int to, const W &weight) {
        matrix[from * this->n_vertices + to] = weight;
    }

    virtual bool hasEdge(const int from, const int to) const {
        return getWeight(from, to) != INVALID_WEIGHT;
    }

    // Breadth first search.
    virtual void BFS(int s) const {
        cout << "Breadth first search: ";

        // Mark all the vertices as not visited
        bool *visited = new bool[this->n_vertices];
        for(int i = 0; i < this->n_vertices; i++)
            visited[i] = false;

        // Create a queue for BFS
        list<int> queue;

        // Mark the current node as visited and enqueue it
        visited[s] = true;
        queue.push_back(s);

        cout << "Nodes: ";
        while(!queue.empty()) {
            // Dequeue a vertex from queue and print it
            s = queue.front();
            cout << s << " ";
            queue.pop_front();

            // Get all adjacent vertices of the dequeued vertex s
            // If a adjacent has not been visited, then mark it visited
            // and enqueue it
            for(int j = 0; j < this->n_vertices; j++) {
                if (matrix[s * this->n_vertices + j] != INVALID_WEIGHT) {
                    if(!visited[j]) {
                        visited[j] = true;
                        queue.push_back(j);
                    }
                }
            }
        }

        cout << endl;
    }

    // Depth first search.
    void DFSUtil(int s, bool visited[]) const {
        // Mark the current node as visited and print it
        visited[s] = true;
        cout << s << " ";

        // Recur for all the vertices adjacent to this vertex
        for (int i = 0; i < this->n_vertices; ++i)
            if (matrix[s * this->n_vertices + i] != INVALID_WEIGHT) {
                if (!visited[i])
                    DFSUtil(i, visited);
            }
    }

    virtual void DFS(int s) const {
        cout << "Depth first search: ";
        // Mark all the vertices as not visited
        bool *visited = new bool[this->n_vertices];
        for (int i = 0; i < this->n_vertices; i++)
            visited[i] = false;

        // Call the recursive helper function to print DFS traversal
        cout << "Nodes: ";
        DFSUtil(s, visited);
        cout << endl;
    }

    // Topological sort.
    void topologicalSortUtil(int s, bool visited[], stack<int> &Stack) const {
        // Mark the current node as visited.
        visited[s] = true;

        // Recur for all the vertices adjacent to this vertex
        for (int i = 0; i < this->n_vertices; ++i)
            if (matrix[s * this->n_vertices + i] != INVALID_WEIGHT) {
                if (!visited[i])
                    topologicalSortUtil(i, visited, Stack);
            }

        // Push current vertex to stack which stores result
        Stack.push(s);
    }

    virtual void topologicalSort() const {
        cout << "Topological sort: ";
        stack<int> Stack;

        // Mark all the vertices as not visited
        bool *visited = new bool[this->n_vertices];
        for (int i = 0; i < this->n_vertices; i++)
            visited[i] = false;

        // Call the recursive helper function to store Topological
        // Sort starting from all vertices one by one
        for (int i = 0; i < this->n_vertices; i++)
            if (visited[i] == false)
                topologicalSortUtil(i, visited, Stack);

        // Print contents of stack
        cout << "Nodes: ";
        while (Stack.empty() == false) {
            cout << Stack.top() << " ";
            Stack.pop();
        }
        cout << endl;
    }

    // Print the matrix.
    friend std::ostream& operator<<(std::ostream& out, GraphMatrix<W>& g) {
        for (int j = 0; j < g.n_vertices; j++) {
            out << '\t' << j;
        }
        out << std::endl;
        for (int i = 0; i < g.n_vertices; i++) {
            out << i;
            for (int j = 0; j < g.n_vertices; j++) {
                out << '\t';
                if (g.getWeight(i, j) == g.INVALID_WEIGHT) {
                    out << '_';
                } else {
                    out << g.getWeight(i, j);
                }
            }
            out << std::endl;
        }
        return out;
    }

protected:
    W *matrix;
    int length;
};


#endif //GRAPH_MATRIXGRAPH_H

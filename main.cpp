#include <iostream>
#include <limits.h>
#include "GraphMatrix.h"
#include "GraphAdjList.h"

using namespace std;

int main() {
    // Initialize Matrix Graph.
    GraphMatrix<int> gm {4, INT_MIN};
    gm.setWeight(0, 1, 1);
    gm.setWeight(0, 2, 2);
    gm.setWeight(1, 2, 3);
    gm.setWeight(2, 0, 4);
    gm.setWeight(2, 3, 5);
    gm.setWeight(3, 3, 6);
    // Print the matrix.
    cout << gm << endl;
    // Perform breadth first search.
    gm.BFS(2);
    // Perform depth first search.
    gm.DFS(2);
    // Perform topological sort
    gm.topologicalSort();

    cout << "---" << endl;

    // Initialize adjacency list.
    GraphAdjList<int> ga {4, INT_MIN};
    ga.setWeight(0, 2, 2);
    ga.setWeight(0, 1, 1);
    ga.setWeight(1, 2, 3);
    ga.setWeight(2, 3, 5);
    ga.setWeight(2, 0, 4);
    ga.setWeight(3, 3, 6);

    // Print the adjacency list.
    cout << ga << endl;
    // Perform breadth first search.
    ga.BFS(2);
    // Perform depth first search.
    ga.DFS(2);
    // Perform topological sort
    ga.topologicalSort();

    return 0;
}
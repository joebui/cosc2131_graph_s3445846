//
// Created by denis on 9/9/15.
//

#ifndef GRAPH_GRAPH_H
#define GRAPH_GRAPH_H

#include <stdexcept>

template<typename W>
class Graph {
public:
    // vertices are numbered from 0 to n_vertices-1 (inclusive)
    Graph(int n_vertices) : n_vertices{n_vertices} {
        if (n_vertices <= 0) {
            throw std::out_of_range("The number of vertices must be greater than 0.");
        }
    }

    virtual W &getWeight(const int from, const int to) const = 0;
    virtual void setWeight(const int from, const int to, const W &weight) = 0;
    virtual bool hasEdge(const int from, const int to) const = 0;
    virtual void BFS(int s) const = 0;
    virtual void DFS(int s) const = 0;
    virtual void topologicalSort() const = 0;
protected:
    int n_vertices;
};


#endif //GRAPH_GRAPH_H
